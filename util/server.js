const http = require('http');

const port = 3000;
const host = '127.0.0.1';

const server = http.createServer((req, res) => {
    if(req.url === '/'){
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        res.write('Hello Dewald!');
        res.end();
    }
});

const startServer = () => {
    server.listen(port, host, () => {
        console.log('Starting server...')
        console.log(`Server running at http://${host}:${port}/ ...`);
    } )
}

module.exports.startServer = startServer;