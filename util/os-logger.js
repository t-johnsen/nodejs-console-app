const os = require('os');


const getOSspecs = () => {
    const totalMemory = (os.totalmem() / 1024 / 1024 / 1024).toFixed(2) + ' GB';
    const freeMemory = (os.freemem() / 1024 / 1024 / 1024).toFixed(2) + ' GB';
    const cores = os.cpus().length;
    const arch = os.arch();
    const platform = os.platform();
    const release = os.release();
    const user = os.userInfo().username;
    const uptime = os.uptime();

    const returnString = `TOTAL MEMORY: ${totalMemory}\nFREE MEMORY: ${freeMemory}\nCORES: ${cores}\nARCHITECTURE: ${arch}\nPLATFORM: ${platform}\nRELEASE: ${release}\nUSER: ${user}\nUPTIME: ${uptime}`;
    return returnString;
}

module.exports.getOSspecs = getOSspecs;
