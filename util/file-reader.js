const fs = require('fs').promises;

const readFile = async () => {
    let file = null;
    try {
        file  = await fs.readFile('./package.json', 'utf8');
    } catch (error) {
        console.log(error);
    }
    return file;

}

module.exports.readFile = readFile;