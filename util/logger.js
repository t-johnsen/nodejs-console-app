const readline = require('readline');
const osLogger = require('./os-logger');
const fileReader = require('./file-reader');
const server = require('./server');

function printOptions() {
    console.log('Choose one of the options below:');
    console.log('1. Read package.json\n2. Display OS information\n3. Start the HTTP server');
}

const chooseOption = () => {
    rl.question('Select an option: ', answer => {
        optionHandler(parseInt(answer));
        rl.close();
    });
}

const optionHandler = (number) => {
    switch (number) {
        case 1:
            fileReader.readFile().then(res => console.log(res));
            break;
        case 2:
            console.log(osLogger.getOSspecs());
            break;
        case 3:
            server.startServer();
            break;

        default:
            console.log('Not a valid option. Try again.')
            break;
    }
}

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});


module.exports = {
    printOptions: printOptions,
    chooseOption: chooseOption
}

// or --> module.exports.printOptions = printOptions;
// or --> module.exports = printOptions;
