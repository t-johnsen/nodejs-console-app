# NodeJS - Console application
A simple console application build with nodejs.

## Getting started and usage
1. Clone repo to a local directory.
2. Open the project in prefered IDE/code editor in root folder.
3. Run the app.js file with node runtime from the console.
```node app.js```
4. If selecting option number 3 in the console application, you close the server with ```Ctr + c```

## Prerequisites
* [NodeJS]

[//]: #
[NodeJS]: <https://nodejs.org/en/>
